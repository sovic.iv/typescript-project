Feature: PetStore Orders

    Scenario: PetStore Orders
        Given I have Id of a pet
            | PetId   |
            | <PetId> |
        When I try to place an order for purchasing the pet with info
            | Id   | PetId   | Quantity   | ShipDate   | Status   | Complete   |
            | <Id> | <PetId> | <Quantity> | <ShipDate> | <Status> | <Complete> |
        Then Code should be 200
    Example:
            | Id | PetId | Quantity | ShipDate                 | Status | Complete |
            | 0  | 2     | 1        | 2021-01-18T13:13:14.787Z | placed | true     |