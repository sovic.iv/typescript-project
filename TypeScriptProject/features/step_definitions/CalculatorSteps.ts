const { Given, When, Then } = require('cucumber');
import { expect } from "chai";

let calculator: any;
let actual: number;
Given('A calculator', function () {
    // Write code here that turns the phrase above into concrete actions
    calculator = {
        divide(n1: number, n2: number): number {
            return n1 / n2
        }
    }
});
When('I try to divide {int} by {int}', function (int: number, int2: number) {
    // Write code here that turns the phrase above into concrete actions
    actual = calculator.divide(int, int2);
});
Then('The result should be {int}', function (int: number) {
    // Then('The result should be {float}', function (float) {           // Write code here that turns the phrase above into concrete actions
    expect(actual).to.be.equal(int);
});