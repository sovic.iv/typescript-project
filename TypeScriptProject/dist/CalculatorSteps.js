"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _a = require('cucumber'), Given = _a.Given, When = _a.When, Then = _a.Then;
var chai_1 = require("chai");
var calculator;
var actual;
Given('A calculator', function () {
    // Write code here that turns the phrase above into concrete actions
    calculator = {
        divide: function (n1, n2) {
            return n1 / n2;
        }
    };
});
When('I try to divide {int} by {int}', function (int, int2) {
    // Write code here that turns the phrase above into concrete actions
    actual = calculator.divide(int, int2);
});
Then('The result should be {int}', function (int) {
    // Then('The result should be {float}', function (float) {           // Write code here that turns the phrase above into concrete actions
    chai_1.expect(actual).to.be.equal(int);
});
